About page
==========

This is a very simple document.

Создание роли в командной строке системы
----------------------------------------

Чтобы создать роль в командной строке системы, введите следующую команду:

.. code-block:: bash

   createuser test_user
   Shall the new role be a superuser? (y/n) n
   Shall the new role be allowed to create databases? (y/n) n
   Shall the new role be allowed to create more new roles? (y/n) n

Команда задаст ряд вопросов, которые определят начальные привелегии.

.. warning::
   Далее последует пример кода с .env-файлом

.. code-block:: python

   import environ
 
   env = environ.Env()
   # reading .env file
   environ.Env.read_env()
  
   # Raises django's ImproperlyConfigured exception if SECRET_KEY not in os.environ
   SECRET_KEY = env("SECRET_KEY")
  
   DATABASES = {
       'default': {
           'ENGINE': 'django.db.backends.postgresql_psycopg2',
           'NAME': env("DATABASE_NAME"),
           'USER': env("DATABASE_USER"),
           'PASSWORD': env("DATABASE_PASSWORD"),
           'HOST': env("DATABASE_HOST"),
           'PORT': env("DATABASE_PORT"),
       }
   }

Вот так, как-то.

